import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store/store';
import Form from './containers/Form';
import List from './containers/List';
import Error from './containers/Error';

import './style/custom.css';

const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <div className="container">
      <Form />
      <List />
      <Error />
    </div>
  </Provider>,
  document.getElementById('root'));
