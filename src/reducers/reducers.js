import _ from 'lodash';
import { initialState } from '../store/store';

function reducers(state = {}, action) {
  switch (action.type) {

    case 'REQUEST_LIST':
      return _.extend({}, state, {
        query: action.query,
        status: 'PENDING',
        results: []
      });

    case 'PROCESS_LIST':
      return _.extend({}, state, {
        status: 'DONE',
        results: action.results
      });

    case 'CLEAR_LIST':
      console.log(initialState);
      return _.extend({}, initialState);

    case 'ERROR':
      return _.assign({}, state, {
        status: action.error,
        results: []
      });

    default:
      return state;
  }
}

export default reducers;