import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchList, clearList } from '../actions/actions';

export class Form extends Component {

  onSubmit(event) {
    event.preventDefault();
    if (this.textInput.value) {
      this.props.onSubmit(this.textInput.value);
    }
  };

  onClear(event) {
    event.preventDefault();
    this.textInput.value = '';
    this.props.onClear();
  };

  render() {
    const ref = (input) => this.textInput = input;
    const submitClassName = 'button is-primary' + (this.props.isSubmitLoading ? ' is-loading' : '');
    const clearClassName = 'button' + (this.props.isClearDisabled ? ' is-disabled' : '');

    return (
      <form className="control is-grouped" onSubmit={this.onSubmit.bind(this)}>

        <p className="control is-expanded has-addons">
          <input className="input is-expanded" autoComplete="off" placeholder="Repo name" type="text" ref={ref}/>
          <button className={submitClassName}>
            <span className="icon">
              <i className="fa fa-search"></i>
            </span>
            <span>Search</span>
          </button>
        </p>

        <button className={clearClassName} onClick={this.onClear.bind(this)}>
            <span className="icon">
              <i className="fa fa-times"></i>
            </span>
          <span>Clear</span>
        </button>

      </form>
    );
  }
}

const mapStateToProps = (store) => {
  return {
    isSubmitLoading: store.status === 'PENDING',
    isClearDisabled: !store.query
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onSubmit: (value) => dispatch(fetchList(value)),
    onClear: () => dispatch(clearList())
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Form);
