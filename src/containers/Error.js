import React, { Component } from 'react';
import { connect } from 'react-redux';

class Error extends Component {

  render() {
    if (!this.props.error) return null;

    // Generic error message
    let errorMessage = 'An error occurred while requesting this repo.';

    // Overriding the message for a `404` error
    if (this.props.error.status === 404) {
      errorMessage = 'The repo you are looking for does not exist.';
    }
      return (
        <div className="notification is-danger">{errorMessage}</div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    error: typeof state.status === 'object' ? state.status : false
  };
};

export default connect(
  mapStateToProps
)(Error);
