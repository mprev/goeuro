import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Item from './Item';

export class List extends Component {
  render() {
    if (!this.props.showResults) return null;

    if (this.props.items.length) {
      const items = _.map(this.props.items, (item) => {
        return (
          <Item key={item.id} name={item.name} url={item.html_url}/>
        );
      });
      return (
        <nav className="panel">
          <p className="panel-heading">Repositories</p>
          {items}
        </nav>
      );
    } else if (this.props.status === 'PENDING') {
      return null;
    } else {
      return (
        <div className="notification is-warning">This repo does not have any project yet.</div>
      )
    }
  }
}

const mapStateToProps = state => {
  return {
    // Show results only when there has been a request and the status is not an object (= an error)
    showResults: state.query && typeof state.status !== 'object',
    status: typeof state.status === 'object' ? 'ERROR' : state.status,
    items: state.results || []
  };
};

export default connect(
  mapStateToProps
)(List);
