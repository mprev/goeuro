import React, { Component } from 'react';

class Item extends Component {
  render () {
    return (
      <a className="panel-block" href={this.props.url} target="_blank">
        <span className="panel-icon">
          <i className="fa fa-github"></i>
        </span>
        {this.props.name}
      </a>
    );
  }
}

export default Item;
