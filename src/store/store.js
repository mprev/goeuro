import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import reducers from '../reducers/reducers';

export const initialState = {
  status: '',
  query: '',
  results: []
};

export default function configureStore() {
  return createStore(reducers, initialState, applyMiddleware(thunkMiddleware));
}