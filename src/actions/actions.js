import fetch from 'isomorphic-fetch';

export function fetchList(value) {
  return dispatch => {
    // Before firing the request, clear the store
    dispatch(requestList(value));
    // Fetching the content
    return fetch(`https://api.github.com/users/${value}/repos`)
      .then(response => {
        // Checking the response's status
        if (response.status >= 200 && response.status < 300) {
          // Everything fine, let's continue
          return response.json();
        } else {
          // We got an error!
          throw {
            message: response.statusText,
            status: response.status
          };
        }
      })
      .then(json => dispatch(processList(json)))
      // Error handling
      .catch(error => dispatch(notifyError(error))
    )
  }
}

function processList(results) {
  return {
    type: 'PROCESS_LIST',
    results
  }
}

function notifyError(error) {
  return {
    type: 'ERROR',
    error
  }
}

function requestList(query) {
  return {
    type: 'REQUEST_LIST',
    query
  }
}

export function clearList() {
  return {
    type: 'CLEAR_LIST'
  }
}
