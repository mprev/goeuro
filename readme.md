# Readme

Simple client-side app that lists GitHub repositories for a given user.
It uses React, Redux, Lodash, Isomorphic Fetch, Babel, Webpack and Mocha, Chai and Enzyme for testing.

## Commands

* ```npm start``` starts the project
* ```npm run build``` ready for production
* ```npm run test``` starts the tests

## Notes
Tested *only* on Chrome v54.

## External resources

* [bulma.io](http://bulma.io) CSS framework
* [fontawesome.io](http://fontawesome.io/) 