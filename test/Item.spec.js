import React from 'react';
import { mount, shallow } from 'enzyme';
import { expect } from 'chai';

import Item from '../src/containers/Item';

describe('<Item/>', function () {
  it('should have a link to a repo', function () {
    const wrapper = shallow(<Item/>);
    expect(wrapper.find('a')).to.have.length(1);
  });

  it('should have props for name and url', function () {
    const wrapper = shallow(<Item/>);
    expect(wrapper.props().name).to.be.defined;
    expect(wrapper.props().url).to.be.defined;
  });
});
