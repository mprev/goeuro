import React from 'react';
import { mount, shallow } from 'enzyme';
import { expect } from 'chai';

import { Form } from '../src/containers/Form';

describe('<Form/>', function () {
  it('should have an input for the text', function () {
    const wrapper = shallow(<Form/>);
    expect(wrapper.find('input')).to.have.length(1);
  });

  it('should have a submit and a clear button', function () {
    const wrapper = shallow(<Form/>);
    expect(wrapper.find('button')).to.have.length(2);
  });

  it('On search submit, saves the query string', function () {
    const wrapper = mount(<Form/>);
    const query = 'goeuro';
    wrapper.setState({ query: query });
    wrapper.find('.is-primary').simulate('click');
    expect(wrapper.state('query')).to.equal(query);
  });

});