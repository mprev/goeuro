import React from 'react';
import { mount, shallow } from 'enzyme';
import { expect } from 'chai';
import logger from 'mocha-logger';

import { List } from '../src/containers/List';
import Item from '../src/containers/Item';

describe('<List/>', function () {

  // With results
  logger.log('With Results [1]');

  const propsA = {
    showResults: true,
    status: 'DONE',
    items: [
      { id: 1, name: 'test', html_url: 'test' },
      { id: 2, name: 'test', html_url: 'test' }
    ]
  };
  it('[1] should have a nav', function () {
    const wrapper = shallow(<List showResults={propsA.showResults} status={propsA.status} items={propsA.items}/>);
    expect(wrapper.find('nav')).to.have.length(1);
  });
  it('[1] should have 2 Items', function () {
    const wrapper = shallow(<List showResults={propsA.showResults} status={propsA.status} items={propsA.items}/>);
    expect(wrapper.find(Item)).to.have.length(2);
  });
  
  // With no results
  logger.log('With no results [2]');

  const propsB = {
    showResults: true,
    status: 'DONE',
    items: []
  };
  it('[2] should not have a nav', function () {
    const wrapper = shallow(<List showResults={propsB.showResults} status={propsB.status} items={propsB.items}/>);
    expect(wrapper.find('nav')).to.have.length(0);
  });
  it('[2] should have 0 Items', function () {
    const wrapper = shallow(<List showResults={propsB.showResults} status={propsB.status} items={propsB.items}/>);
    expect(wrapper.find(Item)).to.have.length(0);
  });
  it('[2] should have a div with a message', function () {
    const wrapper = shallow(<List showResults={propsB.showResults} status={propsB.status} items={propsB.items}/>);
    expect(wrapper.find('div')).to.have.length(1);
  });

  // Pending
  logger.log('Request pending [3]');

  const propsC = {
    showResults: true,
    status: 'PENDING',
    items: []
  };
  it('[3] should not have a nav', function () {
    const wrapper = shallow(<List showResults={propsC.showResults} status={propsC.status} items={propsC.items}/>);
    expect(wrapper.find('nav')).to.have.length(0);
  });
  it('[3] should have 0 Items', function () {
    const wrapper = shallow(<List showResults={propsC.showResults} status={propsC.status} items={propsC.items}/>);
    expect(wrapper.find(Item)).to.have.length(0);
  });

});